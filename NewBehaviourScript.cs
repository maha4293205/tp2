using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    Rigidbody rb;
    public float CurrentSpeed;
    public float heavySpeed = 2f;
    public float normalSpeed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        CurrentSpeed = normalSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Mov = new Vector3(0, 0, 1);
        transform.Translate(Mov * CurrentSpeed * Time.deltaTime);
        rb = GetComponent<Rigidbody>();
        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("water"))
            {
            CurrentSpeed = heavySpeed; 
            };
        if(other.gameObject.tag.Equals("coin"))
        {
            other.gameObject.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals("water"))
        { CurrentSpeed = normalSpeed ;}
    }
    /*
    private void OnCollisionEnter(Collision collision)
    {
          rb = GetComponent<Rigidbody>();
         Debug.Log("collision detected");
    }
    private void OnCollisionStay(Collision collision)
    {
        Debug.Log("collision ");
        
    }
    */
}
